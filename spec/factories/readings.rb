FactoryBot.define do
  factory :reading do
    value { Faker::Number.decimal }
    timestamp { Faker::Date.backward }
    sensor { Faker::StarTrek.location }
  end
end