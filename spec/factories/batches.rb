FactoryBot.define do
  factory :batch do
    name { Faker::StarTrek.character }
    temperature { Faker::Number.decimal }
    start { Faker::Date.backward }
    volume { Faker::Number.number(2) }
  end
end