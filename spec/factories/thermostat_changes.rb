FactoryBot.define do
  factory :thermostat_change do
    state { Faker::Stargate.planet }
    timestamp { Faker::Date.backward }
  end
end