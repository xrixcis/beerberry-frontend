require 'rails_helper'

RSpec.describe 'Readings API' do
  # Initialize the test data
  let!(:token) { create_token }
  let!(:batch) { create(:batch) }
  let!(:readings) { create_list(:reading, 20, batch_id: batch.id) }
  let(:batch_id) { batch.id }
  let(:id) { readings.first.id }
  let(:headers) { { Authorization: 'Token token=' + token } }

  # Test suite for GET /batches/:batch_id/readings
  describe 'GET /batches/:batch_id/readings' do
    before { get "/batches/#{batch_id}/readings", params: nil, headers: headers }

    context 'when reading exists' do
      let(:readings_by_timestamp) do
        readings.sort_by(&:timestamp).collect do |r|
          { 'id' => r.id, 'timestamp' => r.timestamp.as_json }
        end
      end

      it 'returns status code 200' do
        expect(response).to have_http_status(200)
      end

      it 'returns all readings' do
        expect(json.size).to eq(20)
      end

      it 'orders the readings by timestamp' do
        result_readings = json.collect { |r| r.slice('id', 'timestamp') }
        expect(result_readings).to eq(readings_by_timestamp)
      end
    end

    context 'when batch does not exist' do
      let(:batch_id) { 0 }

      it 'returns status code 404' do
        expect(response).to have_http_status(404)
      end

      it 'returns a not found message' do
        expect(response.body).to match(/Couldn't find Batch/)
      end
    end

    context 'when authorization is missing' do
      let(:headers) { nil }

      it 'returns 401' do
        expect(response).to have_http_status(401)
      end
    end
  end

  # Test suite for GET /batches/:batch_id/readings/:id
  describe 'GET /batches/:batch_id/readings/:id' do
    before { get "/batches/#{batch_id}/readings/#{id}", params: nil, headers: headers }

    context 'when reading exists' do
      it 'returns status code 200' do
        expect(response).to have_http_status(200)
      end

      it 'returns the reading' do
        expect(json['id']).to eq(id)
      end
    end

    context 'when reading does not exist' do
      let(:id) { 0 }

      it 'returns status code 404' do
        expect(response).to have_http_status(404)
      end

      it 'returns a not found message' do
        expect(response.body).to match(/Couldn't find Reading/)
      end
    end

    context 'when authorization is missing' do
      let(:headers) { nil }

      it 'returns 401' do
        expect(response).to have_http_status(401)
      end
    end
  end
end