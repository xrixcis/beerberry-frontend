require 'rails_helper'

RSpec.describe 'Main app controller', type: :request do
  # initialize test data
  let!(:user) { create :user }
  let!(:token) { create_token }
  let(:headers) { { Authorization: 'Token token=' + token } }
  let(:password) { 'password' }

  # Test suite for POST /login
  describe 'POST /login' do
    # make HTTP get request before each example
    before { post '/login', params: { name: user.name, password: password } }

    it 'returns status code 200' do
      expect(response).to have_http_status(200)
    end

    it 'returns the token' do
      expect(json).not_to be_empty
      expect(json['token']).not_to be_empty
    end

    context 'when password is bad' do
      let(:password) { 'bad' }

      it 'returns 401' do
        expect(response).to have_http_status(401)
      end
    end
  end

  # Test suite for GET /
  describe 'Authentication ' do
    before { delete '/logout', params: nil, headers: headers }

    context 'when authorization is ok' do
      it 'returns status code 200' do
        expect(response).to have_http_status(200)
      end
    end

    context 'when authorization is missing' do
      let(:headers) { nil }
      it 'returns status code 401' do
        expect(response).to have_http_status(401)
      end
    end

    context 'when authorization is expired' do
      let(:token) { create_token 1.minute.ago }
      it 'returns status code 401' do
        expect(response).to have_http_status(401)
      end
    end
  end
end