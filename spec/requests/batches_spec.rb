require 'rails_helper'

RSpec.describe 'Batches API', type: :request do
  # initialize test data
  let!(:token) { create_token }
  let!(:batches) { create_list(:batch, 10) }
  let(:batch_id) { batches.first.id }
  let(:headers) { { Authorization: 'Token token=' + token } }

  # Test suite for GET /batches
  describe 'GET /batches' do
    # make HTTP get request before each example
    before { get '/batches', params: nil, headers: headers }

    it 'returns batches' do
      expect(json).not_to be_empty
      expect(json.size).to eq(10)
    end

    it 'returns status code 200' do
      expect(response).to have_http_status(200)
    end

    context 'when authorization is missing' do
      let(:headers) { nil }

      it 'returns 401' do
        expect(response).to have_http_status(401)
      end
    end
  end

  # Test suite for GET /batches/:id
  describe 'GET /batches/:id' do
    before { get "/batches/#{batch_id}", params: nil, headers: headers }

    context 'when the record exists' do
      it 'returns the batch' do
        expect(json).not_to be_empty
        expect(json['id']).to eq(batch_id)
      end

      it 'returns status code 200' do
        expect(response).to have_http_status(200)
      end
    end

    context 'when the record does not exist' do
      let(:batch_id) { 100 }

      it 'returns status code 404' do
        expect(response).to have_http_status(404)
      end

      it 'returns a not found message' do
        expect(response.body).to match(/Couldn't find Batch/)
      end
    end

    context 'when authorization is missing' do
      let(:headers) { nil }

      it 'returns 401' do
        expect(response).to have_http_status(401)
      end
    end
  end

  # Test suite for POST /batches
  describe 'POST /batches' do
    # valid payload
    let(:valid_attributes) do
      { name: 'StarBeer', temperature: '25', start: Date.today, volume: 40 }
    end

    context 'when the request is valid' do
      before { post '/batches', params: valid_attributes, headers: headers }

      it 'creates a batch' do
        expect(json['name']).to eq('StarBeer')
      end

      it 'returns status code 201' do
        expect(response).to have_http_status(201)
      end
    end

    context 'when the request is invalid' do
      before { post '/batches', params: { name: 'Foobar' }, headers: headers }

      it 'returns status code 422' do
        expect(response).to have_http_status(422)
      end

      it 'returns a validation failure message' do
        expect(response.body)
          .to match(/Validation failed: Temperature can't be blank/)
      end

      context 'when authorization is missing' do
        let(:headers) { nil }

        it 'returns 401' do
          expect(response).to have_http_status(401)
        end
      end
    end
  end

  # Test suite for PUT /batches/:id
  describe 'PUT /batches/:id' do
    let(:name) { 'Dark stout' }
    let(:volume) { 20 }
    let(:valid_attributes) { { name: name, volume: volume } }

    context 'when the record exists' do
      before { put "/batches/#{batch_id}", params: valid_attributes, headers: headers }

      it 'updates the record' do
        updated = Batch.find(batch_id)
        expect(updated.name).to eq name
        expect(updated.volume).to eq volume
      end

      it 'returns status code 204' do
        expect(response).to have_http_status(204)
      end

      context 'when authorization is missing' do
        let(:headers) { nil }

        it 'returns 401' do
          expect(response).to have_http_status(401)
        end
      end
    end
  end

  # Test suite for DELETE /batches/:id
  describe 'DELETE /batches/:id' do
    before { delete "/batches/#{batch_id}", params: nil, headers: headers }

    it 'returns status code 204' do
      expect(response).to have_http_status(204)
    end

    context 'when authorization is missing' do
      let(:headers) { nil }

      it 'returns 401' do
        expect(response).to have_http_status(401)
      end
    end
  end
end