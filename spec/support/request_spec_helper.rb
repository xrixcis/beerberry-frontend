require 'json_web_token'

module RequestSpecHelper
  # Parse JSON response to ruby hash
  def json
    JSON.parse(response.body)
  end

  def create_token(exp = 24.hours.from_now)
    JsonWebToken.encode({ name: 'foo' }, exp)
  end
end