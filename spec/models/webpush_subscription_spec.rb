require 'rails_helper'

RSpec.describe WebpushSubscription, type: :model do
  it { should validate_presence_of(:endpoint) }
  it { should validate_presence_of(:auth) }
  it { should validate_presence_of(:p256dh) }
end
