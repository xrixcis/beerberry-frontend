require 'rails_helper'

RSpec.describe Reading, type: :model do
  it { should belong_to(:batch) }

  it { should validate_presence_of(:value) }
  it { should validate_presence_of(:timestamp) }
end
