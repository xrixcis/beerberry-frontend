require 'rails_helper'

RSpec.describe ThermostatChange, type: :model do
  it { should belong_to(:batch) }

  it { should validate_presence_of(:state) }
  it { should validate_presence_of(:timestamp) }
end
