require 'rails_helper'

RSpec.describe Batch, type: :model do
  it { should have_many(:readings).dependent(:destroy) }

  it { should validate_presence_of(:name) }
  it { should validate_presence_of(:temperature) }
  it { should validate_presence_of(:start) }
  it { should validate_presence_of(:volume) }
end
