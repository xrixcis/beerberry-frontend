class ApplicationController < ActionController::Base

  include ActionController::HttpAuthentication::Token::ControllerMethods

  include Response
  include ExceptionHandler
  include Authorization

  skip_forgery_protection

  before_action :require_login

  private

  def parse_time(time, default)
    if time.nil?
      default
    else
      Time.zone.parse(time)
    end
  end

  def time_range
    from = parse_time(params[:from], Time.zone.at(0))
    to = parse_time(params[:to], Time.zone.now)
    from..to
  end
end
