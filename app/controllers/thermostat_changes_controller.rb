class ThermostatChangesController < ApplicationController

  before_action :set_batch

  def index
    json_response(@batch.thermostat_changes.where(timestamp: time_range).order(:timestamp))
  end

  private

  def set_batch
    @batch = Batch.find(params[:batch_id])
  end
end
