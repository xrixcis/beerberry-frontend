module ExceptionHandler
  # provides the more graceful `included` method
  extend ActiveSupport::Concern

  included do
    rescue_from ActiveRecord::RecordNotFound do |e|
      json_response({ message: e.message }, :not_found)
    end

    rescue_from ActiveRecord::RecordInvalid do |e|
      puts(e.message)
      json_response({ message: e.message }, :unprocessable_entity)
    end

    rescue_from JWT::DecodeError do |e|
      puts "JWT decoding error: #{e.message}"
      json_response({ message: 'Token error' }, :unauthorized)
    end
  end
end