require 'digest'

class NotificationController < ApplicationController
  def register
    data = subscription_params
    # hash the data and only create a new subscription if we don't already know it
    hash = Digest::SHA2.hexdigest data.to_s
    @notification = WebpushSubscription.find_by(subscription_hash: hash)
    @notification = WebpushSubscription.create!(data.merge(subscription_hash: hash)) if @notification.nil?
    json_response(@notification, :created)
  end

  private

  def subscription_params
    # whitelist params
    req = params.permit(:endpoint, keys: [:auth, :p256dh])
    {
      endpoint: req[:endpoint],
      auth: req[:keys][:auth],
      p256dh: req[:keys][:p256dh]
    }
  end
end
