require 'json_web_token'

class MainController < ApplicationController
  skip_before_action :require_login, only: [:login, :index], raise: false

  def index; end

  def login
    user = User.valid_login?(login_params[:name], login_params[:password])
    if user
      token = JsonWebToken.encode(
          { name: user.name },
          Rails.configuration.jwt_token_expiry.hours.from_now)

      render json: { token: token }
    else
      render_unauthorized('Error with your login or password')
    end
  end

  def logout
    # should invalidate the JWT here, but invalidation infrastructure
    # is not necessary for now - do nothing
    head :ok
  end

  private

  def login_params
    params.permit(:name, :password)
  end
end
