class BatchesController < ApplicationController
  before_action :set_batch, only: [:show, :update, :destroy]

  def index
    @batches = Batch.order(start: :desc)
    json_response(@batches)
  end

  def create
    @batch = Batch.create!(batch_params)
    json_response(@batch, :created)
  end

  def show
    json_response(@batch)
  end

  def update
    @batch.update(batch_params)
    head :no_content
  end

  def destroy
    @batch.destroy
    head :no_content
  end

  private

  def batch_params
    # whitelist params
    params.permit(:start, :name, :temperature, :volume)
  end

  def set_batch
    @batch = if params[:id] == 'last'
               Batch.order(:start).last
             else
               Batch.find(params[:id])
             end
  end
end
