import Vue from 'vue/dist/vue.esm'
import Vuex from 'vuex/dist/vuex.esm'
import axios from 'axios/dist/axios';
import VuexPersistence from "vuex-persist";

Vue.use(Vuex)

// store some of the vuex state in session storage
const persist = new VuexPersistence({
    storage: window.sessionStorage,
    reducer: (state) => ({token: state.token})
});

// dispatch the registerLogoutHandler action when token changes
const logoutPlugin = store => {
    store.watch(state => state.token,
            () => store.dispatch('registerLogoutHandler'))
};

function findToken(ary) {
    return ary.findIndex(proto => proto.match(/^token-/) !== null);
}

/**
 * Initializes ActionCable so that the token is included as a subprotocol of the websocket connection
 *
 * Note: the subprotocol can't contain = in Chrome, so use - as separator
 *
 * @param token authentication token
 */
function initWebsocketToken(token) {
    let protocols = ActionCable.INTERNAL.protocols;
    let idx;
    while((idx = findToken(protocols)) >= 0) {
        protocols.splice(idx, 1);
    }
    protocols.splice(1, 0, 'token-' + token);
}

export default new Vuex.Store({
    plugins: [logoutPlugin, persist.plugin],
    state: {
        credsExpired: false,
        error: null,
        batch: null,
        temperature: 0,
        token: null
    },
    actions: {
        registerLogoutHandler(context) {
            let axios = context.getters.axios;
            if (axios) {
                axios.interceptors.response.use(
                    response => response,
                    error => {
                        if (error.response && error.response.status === 401) {
                            context.commit('setCredsExpired', true);
                            context.commit('logOut');
                        }
                        return Promise.reject(error);
                    }
                );
            }
        }
    },
    getters: {
        axios: state => {
            if (state.token !== null) {
                // initialize the websocket connection too
                initWebsocketToken(state.token);

                return axios.create({
                    headers: {'Authorization': 'Token token=' + state.token}
                });
            } else {
                return null;
            }
        }
    },
    mutations: {
        logOut(state) {
            state.token = null;
        },
        setCredsExpired(state, expired) {
            state.credsExpired = expired;
        },
        setError(state, error) {
            state.error = error.toString();
            console.error(error);
        },
        clearError(state) {
            state.error = null;
        },
        setTemperature(state, temperature) {
            state.temperature = temperature
        },
        selectBatch(state, batch) {
            state.batch = batch;
        },
        reloadBatch(state) {
            let batch = state.batch;
            state.batch = null;
            state.batch = batch;
        },
        setAuthToken(state, token) {
            state.token = token;
        }
    }
})