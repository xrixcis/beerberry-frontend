import Vue from 'vue/dist/vue.esm'
import App from '../components/app.vue'
import store from '../store/store'
import axios from 'axios/dist/axios';

// initialize axios base url
axios.defaults.baseURL = process.env.RAILS_RELATIVE_URL_ROOT;

document.addEventListener('DOMContentLoaded', () => {
    let app = new Vue({
        el: '#app',
        store,
        data: {
            error: false
        },
        components: {
            App
        }
    });
})
