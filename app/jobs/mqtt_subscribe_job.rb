require 'mqtt_client'

module MQTT
  class Error < ::StandardError
  end
end

class MqttSubscribeJob < ApplicationJob
  include MQTTClient
  queue_as :default

  retry_on MQTT::Error, wait: 60.seconds, attempts: 10_000_000

  def perform(*args)
    conf = Rails.configuration.messaging
    begin
      connect do |client|
        client.subscribe(conf[:temperature_topic] => conf[:qos], conf[:thermostat_state_topic] => conf[:qos])
        client.get do |topic, message|
          batch = Batch.order(:start).last
          unless batch.nil?
            if (topic == conf[:thermostat_state_topic])
              thermostat_change(message, batch)
            else
              sensor_reading(topic, message, batch)
            end
          end
        end
      end
    rescue MQTT::Exception
      raise MQTT::Error
    end
  end

  private

  def thermostat_change(message, batch)
    time, state = message.split
    state = batch.thermostat_changes.create(timestamp: DateTime.parse(time), state: state)
    EventChannel.broadcast_to(batch, type: :state, data: state)
  end

  def sensor_reading(topic, message, batch)
    sensor = topic.split('/').last
    time, temp = message.split
    if temp != 'nan'
      reading = batch.readings.create(timestamp: DateTime.parse(time), value: temp, sensor: sensor)
      EventChannel.broadcast_to(batch, type: :reading, data: reading)
    end
  end

end
