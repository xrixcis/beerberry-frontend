require 'json_web_token'

module ApplicationCable
  class Connection < ActionCable::Connection::Base
    identified_by :current_user

    def connect
      token_str = get_token

      reject_unauthorized_connection if token_str.nil?

      token = token_str.sub /token-/, ''

      begin
        jwt = JsonWebToken.decode(token)
        self.current_user = jwt['name']
      rescue JWT::DecodeError
        reject_unauthorized_connection
      end
    end

    private

    def get_token
      request
          .headers[:HTTP_SEC_WEBSOCKET_PROTOCOL]
          .split(' ')
          .find { |proto| /^token-/.match? proto }
    end
  end
end
