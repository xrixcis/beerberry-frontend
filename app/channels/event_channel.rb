class EventChannel < ApplicationCable::Channel
  def subscribed
    batch = Batch.find(params[:batchId])
    stream_for batch
  end
end