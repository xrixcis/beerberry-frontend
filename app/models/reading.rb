class Reading < ApplicationRecord
  belongs_to :batch

  validates_presence_of :value, :timestamp

  after_save :check_coolant_temperature, if: :coolant?

  private

  def coolant?
    sensor == Rails.configuration.sensors[:coolant]
  end

  def check_coolant_temperature
    target_sensor = Rails.configuration.sensors[:target]
    min_difference = Rails.configuration.sensors[:min_difference]
    target = Reading.where(sensor: target_sensor).order(:timestamp).last
    if !target.nil? && value > target.value - min_difference
      WebpushSubscription.all.each { |sub| sub.send_message(value) }
    end
  end
end
