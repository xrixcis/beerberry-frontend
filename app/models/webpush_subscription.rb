require 'webpush'

class WebpushSubscription < ApplicationRecord
  validates_presence_of :endpoint, :auth, :p256dh

  def send_message(message)
    begin
      Webpush.payload_send(
        message: message.to_s,
        endpoint: endpoint,
        p256dh: p256dh,
        auth: auth,
        vapid: {
          public_key: Rails.application.credentials[:vapid][:public],
          private_key: Rails.application.credentials[:vapid][:private]
        }
      )
    rescue Webpush::ResponseError => e
      Rails.logger.warn "Could not send notification to subscription: #{e}"
      delete
    end
  end
end
