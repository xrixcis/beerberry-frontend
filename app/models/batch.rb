class Batch < ApplicationRecord

  include MQTTClient

  has_many :readings, dependent: :destroy
  has_many :thermostat_changes, dependent: :destroy

  validates_presence_of :name, :temperature, :start, :volume

  after_save :send_thermostat_temp, if: :latest?

  private

  def latest?
    Batch.order(:start).last == self
  end

  def send_thermostat_temp
    connect do |client|
      client.publish(Rails.configuration.messaging[:control_topic],
                     temperature.to_s, true, Rails.configuration.messaging[:qos])
    end
  end
end
