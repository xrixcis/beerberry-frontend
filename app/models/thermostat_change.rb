class ThermostatChange < ApplicationRecord
  belongs_to :batch

  validates_presence_of :state, :timestamp
end
