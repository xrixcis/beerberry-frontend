const { environment } = require('@rails/webpacker')
const vue =  require('./loaders/vue')
const webpack = require('webpack')

environment.loaders.append('vue', vue)
module.exports = environment

// Add an additional plugin of your choosing : ProvidePlugin
environment.plugins.prepend(
    'Ignore',
    new webpack.IgnorePlugin(/^\.\/locale$/, /moment$/)
)
