Rails.application.routes.draw do
  get 'main/index'
  post   '/login'       => 'main#login'
  delete '/logout'      => 'main#logout'
  post 'notification/register'


  mount ActionCable.server => '/cable'
  resources :batches do
    resources :readings, only: [:show, :index]
    resources :thermostat_changes, only: [:index]
  end

  root 'main#index'
end
