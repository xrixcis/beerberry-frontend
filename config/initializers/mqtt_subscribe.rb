Rails.application.config.after_initialize do
  # start the message listening background job
  MqttSubscribeJob.perform_later
end
