module MQTTClient

  def connect(&block)
    conf = Rails.configuration.messaging
    MQTT::Client.connect(conf[:host],
                         port: conf[:port],
                         clean_session: conf[:clean_session],
                         client_id: conf[:client_id],
                         username: Rails.application.credentials.mqtt[:user],
                         password: Rails.application.credentials.mqtt[:pass], &block)
  end
end