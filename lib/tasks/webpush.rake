require 'webpush'

namespace :webpush do
  desc "Generates the public/private keypair used for webpush notifications"
  task :generate_key do
    key = Webpush.generate_key
    puts 'EDITOR=nano bin/rails credentials:edit'
    puts 'vapid:'
    puts "  public: #{key.public_key}"
    puts "  private: #{key.private_key}"
  end

end
