# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 2019_04_25_172951) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "batches", force: :cascade do |t|
    t.string "name"
    t.date "start"
    t.float "temperature"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer "volume"
  end

  create_table "readings", force: :cascade do |t|
    t.datetime "timestamp"
    t.float "value"
    t.bigint "batch_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "sensor"
    t.index ["batch_id"], name: "index_readings_on_batch_id"
  end

  create_table "thermostat_changes", force: :cascade do |t|
    t.datetime "timestamp"
    t.string "state"
    t.bigint "batch_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["batch_id"], name: "index_thermostat_changes_on_batch_id"
  end

  create_table "users", force: :cascade do |t|
    t.string "name"
    t.string "password_digest"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["name"], name: "index_users_on_name", unique: true
  end

  create_table "webpush_subscriptions", force: :cascade do |t|
    t.string "endpoint"
    t.string "auth"
    t.string "p256dh"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "subscription_hash"
    t.index ["subscription_hash"], name: "index_webpush_subscriptions_on_subscription_hash", unique: true
  end

  add_foreign_key "readings", "batches"
  add_foreign_key "thermostat_changes", "batches"
end
