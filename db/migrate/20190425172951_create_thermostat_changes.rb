class CreateThermostatChanges < ActiveRecord::Migration[5.2]
  def change
    create_table :thermostat_changes do |t|
      t.timestamp :timestamp
      t.string :state
      t.references :batch, foreign_key: true

      t.timestamps
    end
  end
end
