class AddSensorToReadings < ActiveRecord::Migration[5.2]
  def change
    add_column :readings, :sensor, :string
  end
end
