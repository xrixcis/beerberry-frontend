class AddHashToWebpushSubscriptions < ActiveRecord::Migration[5.2]
  def change
    add_column :webpush_subscriptions, :subscription_hash, :string
    add_index(:webpush_subscriptions, :subscription_hash, unique: true)
  end
end
