class CreateReadings < ActiveRecord::Migration[5.2]
  def change
    create_table :readings do |t|
      t.datetime :timestamp
      t.float :value
      t.references :batch, foreign_key: true

      t.timestamps
    end
  end
end
