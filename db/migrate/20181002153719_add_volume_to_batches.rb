class AddVolumeToBatches < ActiveRecord::Migration[5.2]
  def change
    add_column :batches, :volume, :integer
  end
end
